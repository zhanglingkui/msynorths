# coding:utf-8
import networkx as nx
import matplotlib
import matplotlib.pyplot as plt
import sys
matplotlib.rcParams['pdf.fonttype']=42
plt.switch_backend('agg')
def pict_net(total_modu_list,list_index,need_list,out_name):
    G = nx.Graph()  
    la_dict={}
    index=0
    for i in need_list:
        la_dict[str(index)]=i
        index+=1
    G.add_edges_from(total_modu_list[list_index])
    plt.figure(figsize=(15,8))
    pos=nx.spring_layout(G)
    nx.draw_networkx(G,pos,node_size=500)
    str_label=''
    for key,value in la_dict.items():
        str_label+=(key+':'+value+'\n')
    # plt.axis('off')
    plt.ylabel(str_label,rotation=0,labelpad=30,position=(10,0),fontsize=10,linespacing=1.5)
    plt.gca().spines["top"].set_alpha(0.0)
    plt.gca().spines["bottom"].set_alpha(0)
    plt.gca().spines["left"].set_alpha(0)
    plt.gca().spines["right"].set_alpha(0)
    plt.savefig(out_name)

gene_id=sys.argv[3]
input_file=open(sys.argv[1],'r')
total_modu_list=[]
for line in input_file:
    line_list=line.strip().split('\t')
    modu_list=[]
    for i in line_list:
        i_list=i.split(':')
        modu_list.append(i_list)
        # print(i_list)
        # G.add_edge(int(i_list[0]), int(i_list[1]))
    total_modu_list.append(modu_list)
file1=open(sys.argv[2],'r')  
index=-1
need_list=[]
for line in file1: 
    index+=1       
    line_list=line.strip().split('\t')
    for i in line_list:
        # i_list=i.strip().split('=')
        if i == gene_id:
            list_index1=index
            need_list1=line_list
            pict_net(total_modu_list,list_index1,need_list1,str(index)+'line.pdf')
            exit()
            
