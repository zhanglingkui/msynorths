# -*- coding=utf-8 -*-
# 2023.05.20
# @zlk
# 选一个参考基因组，进行共线性
import os
import sys
# import multiprocessing
import pick_pep
import check_file
import time
import pick_tandem
import synteny2
import cluster_syn_gene

def normal_workflow(fa_gff_file,out_folder,threads,tools,evalue,gap_num,flank_gene_num,seg_num,best_syn_ration,identity,coverage):

    print(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())+'\tStart to initialize the working directory and extract the protein sequence.')
    check_file.check_file(fa_gff_file,out_folder,0)

    pick_pep.run_pick_pep(threads,out_folder) 

    print(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())+'\tStart to self blast.')
    pick_tandem.self_blast(out_folder,tools,threads,evalue,)

    print(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())+'\tStart to pick tandem array.')
    pick_tandem.run_pick_tandem(out_folder,threads)

    print(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())+'\tStart to together blastp.')
    synteny2.together_blast(out_folder,tools,threads,evalue,)

    print(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())+'\tStart to syntenic analyse.')
    synteny2.run_synteny(out_folder,20,gap_num,flank_gene_num,seg_num,best_syn_ration,evalue,identity,coverage)

    print(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())+'\tStart to cluster all syntenic gene pairs.')
    Total_syn_list,Total_net_list=cluster_syn_gene.cluster_gene_pairs(out_folder,out_folder+'/total_temp_syntenic_gene_pairs.txt',out_folder+'/total_temp_syntenic_network.txt')
    cluster_syn_gene.MCL_cluster(Total_syn_list,Total_net_list,out_folder+'/Total_species_syntenic_gene_pairs.txt',threads)
    
    print(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())+'\tAll jobs have been completed.')


def run_mSynOrths(fa_gff_file,out_folder,threads,tools,evalue,gap_num,flank_gene_num,seg_num,best_syn_ration,identity,coverage):
    if os.path.exists(out_folder):
        if len(os.listdir(out_folder))!=0:
            print('There are files in the output folder, please change the output folder!!!')
            sys.exit()
    else:
        os.mkdir(out_folder)
    normal_workflow(fa_gff_file,out_folder,threads,tools,evalue,gap_num,flank_gene_num,seg_num,best_syn_ration,identity,coverage)

def run_continue(fa_gff_file,out_folder,threads,tools,evalue,gap_num,flank_gene_num,seg_num,best_syn_ration,identity,coverage):
    ### Look for lastest create file ####
    last_file=''
    last_file_time=0
    for mSynF in os.listdir(out_folder):
        if os.path.isdir(mSynF):
            for new_file in os.listdir(out_folder+'/'+mSynF):
                if os.path.getmtime(new_file)>last_file_time:
                    last_file_time=os.path.getmtime(out_folder+'/'+mSynF+'/'+new_file)
                    last_file=out_folder+'/'+mSynF+'/'+new_file
        # else:
        #     if os.path.getmtime(out_folder+'/'+mSynF)>last_file_time:
        #         last_file_time=os.path.getmtime(out_folder+'/'+mSynF)
        #         last_file=out_folder+'/'+mSynF
    # print(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())+'\tDelete the last created file.')
    # commond='rm '+last_file
    # os.system(commond)

    print(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())+'\tDetect missing files from begining and continue running the program from the breakpoint.')
    normal_workflow(fa_gff_file,out_folder,threads,tools,evalue,gap_num,flank_gene_num,seg_num,best_syn_ration,identity,coverage)


def run_add_genome(fa_gff_file,out_folder,threads,tools,evalue,gap_num,flank_gene_num,seg_num,best_syn_ration,identity,coverage):
    fold_num=0
    for mSynF in os.listdir(out_folder):
        if mSynF.startswith('mSynF') and  os.path.isdir(mSynF):
            fold_num+=1
    in_fa_gff_file=open(fa_gff_file,'r')
    new_add_file=''
    line_num=1
    for line in in_fa_gff_file:
        if line.startswith('##'):
            continue
        else:
            line_num+=1
        if line_num>fold_num:

            new_add_file+=line
    in_fa_gff_file.close()
    print(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())+'\t '+new_add_file +' are newly added genomes.')
    normal_workflow(fa_gff_file,out_folder,threads,tools,evalue,gap_num,flank_gene_num,seg_num,best_syn_ration,identity,coverage)
    # run_mSynOrths(fa_gff_file,out_folder,threads,tools,evalue,gap_num,flank_gene_num)



if __name__ == "__main__":
    run_mSynOrths(sys.argv[1],sys.argv[2],20,sys.argv[3])



