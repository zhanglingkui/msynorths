import collections
import matplotlib.pyplot as plt
import matplotlib
import os
matplotlib.rcParams['pdf.fonttype']=42
def get_pos_dict(pos_file,chr_gap,order_list):
    loop_pos_file=open(pos_file,'r')
    chr_gene_pos_dict=collections.OrderedDict()
    for line in loop_pos_file:
        line_list=line.strip().split('\t')
        if line_list[3] in chr_gene_pos_dict.keys():
            chr_gene_pos_dict[line_list[3]].append([line_list[0],int(line_list[4].split(':')[0])])
        else:
            chr_gene_pos_dict[line_list[3]]=[[line_list[0],int(line_list[4].split(':')[0])]]

    loop_pos_file.close()
    gene_pos_dict={}
    chr_pos_list=[]
    chr_length_dict={}
    chr_start=0
    for key in order_list:
        if key=='':
            continue
        elif key in chr_gene_pos_dict.keys():

            value=chr_gene_pos_dict[key]
        else:
            print(key+' is wrong.')
        index=0
        for i in value:
            index+=1
            # gene_pos_dict[i[0]]=i[1]+chr_start
            gene_pos_dict[i[0]]=index+chr_start
        # chr_pos_list.append([key,chr_start,chr_start+value[-1][1]-value[0][1]])
        # chr_length_dict[key]=value[-1][1]-value[0][1]
        # chr_start+=(value[-1][1]-value[0][1]+chr_gap)
        chr_length_dict[key]=index
        chr_start+=(index+chr_gap)

        # chr_length_dict[key]=1
        # chr_start+=(1+chr_gap)

    return gene_pos_dict,chr_length_dict

def syn_point_pict(pos_file1,pos_file2,syn_file,x_list,y_list,out_name,point_color,x_file,y_file,x_name,y_name):
    gene_pos_dict1,chr_pos_dict1=get_pos_dict(pos_file1,0,x_list)
    gene_pos_dict2,chr_pos_dict2=get_pos_dict(pos_file2,0,y_list)
    fig = plt.figure(figsize=(12, 12))
    # ax1 = fig.add_subplot(111,facecolor='#e8e8e8')
    ax1 = fig.add_subplot(111)
    scatter_x,scatter_y=[],[]
    loop_syn_file=open(syn_file,'r')
    for line in loop_syn_file:
        if line.startswith('#'):
            continue
        line_list=line.strip().split('\t')
        if line_list[0] in gene_pos_dict1.keys() and line_list[1] in gene_pos_dict2.keys():
            scatter_x.append(gene_pos_dict1[line_list[0]])
            scatter_y.append(gene_pos_dict2[line_list[1]])
        if line_list[1] in gene_pos_dict1.keys() and line_list[0] in gene_pos_dict2.keys():
            scatter_x.append(gene_pos_dict1[line_list[1]])
            scatter_y.append(gene_pos_dict2[line_list[0]])
    x_length=0
    xticks_list=[[],[]]

    for chr_name in x_list:
        # if chr_name=='':
        #     continue
        if chr_name in chr_pos_dict1.keys():
            x_length+=chr_pos_dict1[chr_name]
            xticks_list[0].append(x_length)
            xticks_list[1].append(chr_name)
            plt.axvline(x_length,c='#6a737b',lw=1.5,linestyle='--')
        else:
            print(chr_name+' is not in genome!!')
    Y_length=0
    yticks_list=[[],[]]
    for chr_name in y_list:
        if chr_name in chr_pos_dict2.keys():
            Y_length+=chr_pos_dict2[chr_name]
            yticks_list[0].append(Y_length)
            yticks_list[1].append(chr_name)
            plt.axhline(Y_length,c='#6a737b',lw=1.5,linestyle='--')
        else:
            print(chr_name+' is not in genome!!')
        
    loop_syn_file.close()
    ax1.scatter(scatter_x,scatter_y,color=point_color,s=0.2,alpha = 1)

    plt.xlim([0,x_length])
    plt.ylim([0,Y_length])
    plt.yticks(yticks_list[0],labels=yticks_list[1],fontsize=15)
    plt.xticks(xticks_list[0],labels=xticks_list[1],rotation=90,fontsize=15)
    if x_name and x_name!='':
        plt.xlabel(x_name,fontsize=30)
    else:
        plt.xlabel(x_file,fontsize=20)
    if y_name and y_name!='':
        plt.ylabel(y_name,fontsize=30)
    else:
        plt.ylabel(y_file,fontsize=30)
    plt.savefig(out_name,dpi=300)

def run_syn_pict(config_file,msynf,out_name,in_syn_file):
    op_config_file=open(config_file,'r')
    for line in op_config_file:
        line_list=line.strip().split(':')
        if line.startswith('Point_color'):
            point_color=line_list[1].strip()
        elif line.startswith('X-axis'):
            x_file=line_list[1].strip()
        elif line.startswith('X-chr_order'):
            x_list=line_list[1].strip().split(',')
            x_list.remove('')
        elif line.startswith('Y-axis'):
            y_file=line_list[1].strip()
        elif line.startswith('Y-chr_order'):
            y_list=line_list[1].strip().split(',')
            y_list.remove('')
        elif line.startswith('X-name'):
            x_name=line_list[1].strip()
        elif line.startswith('Y-name'):
            y_name=line_list[1].strip()
    op_config_file.close()
    for folder in os.listdir(msynf):
        if folder == x_file:
            pos_file1=msynf+'/'+folder+'/species.gff_pos'
        if folder == y_file:
            pos_file2=msynf+'/'+folder+'/species.gff_pos'
    if in_syn_file:
        syn_file=in_syn_file
    elif int(x_file[5:])>int(y_file[5:]):
        syn_file=msynf+'/'+y_file+'/species'+y_file[5:]+'_species'+x_file[5:]+'.syn.seg'
    elif int(x_file[5:])==int(y_file[5:]):
        syn_file=msynf+'/'+y_file+'/self.syn'
    else:
        syn_file=msynf+'/'+x_file+'/species'+x_file[5:]+'_species'+y_file[5:]+'.syn.seg'
    # print(syn_file)
    syn_point_pict(pos_file1,pos_file2,syn_file,x_list,y_list,out_name,point_color,x_file,y_file,x_name,y_name)

if __name__ == '__main__':
    # import sys
    import argparse
    parser = argparse.ArgumentParser(description='Syn_pict')
    parser.add_argument('-f', type=str,help='Syn_pict config file')
    parser.add_argument('-d', type=str, help='mSynOrths out folder')
    parser.add_argument('-s', type=str, help='syn file')
    parser.add_argument('-o', type=str, help='Out picture file name (.pdf, .png, .svg)')
    args = parser.parse_args()
    if args.f and args.d and args.o:
        run_syn_pict(args.f,args.d,args.o,args.s)
    else:
        print('Incomplete parameters!!!')
    # syn_point_pict(sys.argv[1],sys.argv[2],sys.argv[3])
    
    


