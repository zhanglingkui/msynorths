import sys
import os
import multiprocessing
import markov_clustering as mc
import networkx as nx
import re
import time
def add_gene_dict(genex,geney,tar_dict):
    if genex in tar_dict.keys():
        if geney in tar_dict[genex]:
            a=1
        else:
            tar_dict[genex].append(geney)
    else:
        tar_dict[genex]=[geney]


def cluster_gene_pairs(out_fold,out_file,out_modu_file):
    total_gene_pairs_dict={}
    total_self_gene_pairs_dict={}
    # flank_num_dict={}
    print('Start to find syntenic gene pairs')
    for mSynF in os.listdir(out_fold):
        if mSynF.startswith('mSynF'):
            for syn_seg_file in os.listdir(out_fold+'/'+mSynF):
                if syn_seg_file.endswith('.syn.seg'): 
                    # continue
                    loop_syn_seg_file=open(out_fold+'/'+mSynF+'/'+syn_seg_file,'r')  
                    specie1=syn_seg_file.split('_')[0][7:]
                    specie2=syn_seg_file.split('_')[1][7:-8]
                    for line in loop_syn_seg_file:
                        if line.startswith('##'):
                            continue
                        line_list=line.strip().split('\t')
                        gene1=specie1+':'+line_list[0]
                        gene2=specie2+':'+line_list[1]
                        add_gene_dict(gene1,gene2,total_gene_pairs_dict)
                        add_gene_dict(gene2,gene1,total_gene_pairs_dict)
                    loop_syn_seg_file.close()
                ##########add ortholog############  
                elif syn_seg_file.endswith('.ortho'): 
                    print('xxxxxxxxxx'+syn_seg_file)
                    loop_syn_seg_file=open(out_fold+'/'+mSynF+'/'+syn_seg_file,'r')  
                    specie1=syn_seg_file.split('_')[0][7:]
                    specie2=syn_seg_file.split('_')[1][7:-6]
                    for line in loop_syn_seg_file:
                        if line.startswith('##'):
                            continue
                        line_list=line.strip().split('\t')
                        gene1=specie1+':'+line_list[0]
                        gene2=specie2+':'+line_list[1]
                        add_gene_dict(gene1,gene2,total_gene_pairs_dict)
                        add_gene_dict(gene2,gene1,total_gene_pairs_dict)
                    loop_syn_seg_file.close()  
                
                #############self.syn#############
                # elif syn_seg_file.endswith('self.syn'):
                #     loop_syn_seg_file=open(out_fold+'/'+mSynF+'/'+syn_seg_file,'r')  
                #     for line in loop_syn_seg_file:
                #         line_list=line.strip().split('\t')
                #         add_gene_dict(line_list[0],line_list[1],total_self_gene_pairs_dict)
                #         add_gene_dict(line_list[1],line_list[0],total_self_gene_pairs_dict)
                        # total_self_gene_pairs_dict[line_list[0]]
                
    temp_total_gene_pairs_dict=total_gene_pairs_dict.copy()
    print('Start to cluster syntenic gene family')
    total_syn_list=[]
    total_modu_list=[]
    line_index=0
    temp_list=[]
    for key,value in total_gene_pairs_dict.items():
        if len(value)>80:
            del temp_total_gene_pairs_dict[key]
        # temp_list.append(len(value))
    # print(sorted(temp_list,reverse=True)[:100])
    # sys.exit()
    for key,value in total_gene_pairs_dict.items():
        try:
            del temp_total_gene_pairs_dict[key]
        except KeyError:
            continue
        line_index+=1
        #print(line_index)
        gene_name_list=value+[key]
        gene_pair_list=[]
        for gene in value:
            gene_pair_list.append([key,gene])
            
        for syn_gene in gene_name_list:
            # if syn_gene in temp_total_gene_pairs_dict.keys():
            try:
                for syn_gene2 in temp_total_gene_pairs_dict[syn_gene]:
                    gene_pair_list.append([syn_gene,syn_gene2])
                    if syn_gene2 not in gene_name_list:
                        gene_name_list.append(syn_gene2)
                del temp_total_gene_pairs_dict[syn_gene]
            except KeyError:
                continue
        #    print(len(gene_name_list))
        # gene_name_list=value+[key]
        # gene_name_list=list(set(gene_name_list))
        total_syn_list.append(gene_name_list)
        ### add self syntenic gene pair  to modu#####
        # gene_name_dict={}
        # for gene_name in gene_name_list:
        #     specie=gene_name.split(':')[0]
        #     # gene=gene_name.split(':')[1]
        #     if specie in gene_name_dict.keys():
        #         gene_name_dict[specie].append(gene_name)
        #     else:
        #         gene_name_dict[specie]=[gene_name]
        # for key,value in gene_name_dict.items():
        #     if len(value) > 1:
        #         for i in range(len(value)-1):
        #             for j in range(i+1,len(value)):
        #                 re.findall(r':(.*)',value[i])
        #                 if re.findall(r':(.*)',value[i])[0] in total_self_gene_pairs_dict.keys():
        #                     if re.findall(r':(.*)',value[j])[0] in total_self_gene_pairs_dict[re.findall(r':(.*)',value[i])[0]]:
        #                         modu_list.append([gene_name_list.index(value[i]),gene_name_list.index(value[j])])
        modu_list=[]
        gene_name_dict={}
        gene_index=-1
        for gene_name in gene_name_list:
            gene_index+=1
            gene_name_dict[gene_name]=gene_index
            
        for gene_pair in gene_pair_list:
            add_modu=[gene_name_dict[gene_pair[0]],gene_name_dict[gene_pair[1]]]
            if add_modu not in modu_list and [add_modu[1],add_modu[0]] not in modu_list:
                modu_list.append(add_modu)
        total_modu_list.append(modu_list)
        ###############!!!!!!!!!!!!!!!!!!###############
        # if len(total_modu_list)>200:
        #     break
    wr_out_file=open(out_file,'w')
    index=0
    for gene_list in total_syn_list:
        index+=1
        wr_out_file.write('line:'+str(index)+'l\t')
        for gene_id in gene_list:
            wr_out_file.write(gene_id+'\t')
        wr_out_file.write('\n')
    wr_out_file.close()
    wr_out_modu_file=open(out_modu_file,'w')
    index=0
    for pair_list in total_modu_list:
        index+=1
        wr_out_modu_file.write('line:'+str(index)+'l\t')
        for gene_pair in pair_list:
            wr_out_modu_file.write(str(gene_pair[0])+':'+str(gene_pair[1])+'\t')
        wr_out_modu_file.write('\n')
    wr_out_modu_file.close()
    return total_syn_list,total_modu_list

def MCL_modu_cal(in_gene_list,in_modu_list,big_num):
    out_list=[]
    network = nx.Graph()
    syntenic_modu_dict={}
    for syn_modu_list in in_modu_list:
        add_gene_dict(syn_modu_list[0],syn_modu_list[1],syntenic_modu_dict)
        add_gene_dict(syn_modu_list[1],syn_modu_list[0],syntenic_modu_dict)
    def get_modu_list(temp_modu_list,total_modu_dict):
        out_list=[]
        # for num in temp_modu_list:
        for num,value in total_modu_dict.items():
            for num2 in value:
                if num in temp_modu_list and num2 in temp_modu_list:
                    if [num,num2] not in out_list and [num2,num] not in out_list:
                        out_list.append([num,num2])
        return out_list
    modu_list=[]
    gene_list=[]
    for i in in_modu_list:
        if i[0] not in modu_list:
            modu_list.append(i[0])
        if i[1] not in modu_list:
            modu_list.append(i[1])
    for i in modu_list:
        gene_list.append(in_gene_list[i]) 
    # 获取边列表edges_list
    network.add_edges_from(in_modu_list)
    # then get the adjacency matrix (in sparse form)
    matrix = nx.to_scipy_sparse_matrix(network)
    this_result = mc.run_mcl(matrix, inflation=2.4)
    big_clusters = mc.get_clusters(this_result)
    
    out_cluster=big_clusters
    for split_modu_list in sorted(out_cluster,key=lambda m:len(m)):
        if len(split_modu_list)<3:
            for i in split_modu_list:
                try:
                    for x in syntenic_modu_dict[i]:
                        if x in split_modu_list:
                            continue
                        for index_num in range(len(out_cluster)):
                            if x in out_cluster[index_num] and len(out_cluster[index_num])>2:
                                out_cluster[index_num]+=split_modu_list
                except KeyError:
                    continue
    out_gene=[]
    for i_list in out_cluster:
        if len(i_list)<3:
            continue
        out_list.append(list(set(i_list)))
        out_gene_list=[]
        for i in list(set(i_list)):
            out_gene_list.append(gene_list[i])
        out_gene.append(out_gene_list)
            
    return [out_list,out_gene]

def MCL_cluster(total_syn_list,total_modu_list,right_file,threads_num):
    ##############
    big_num=len(sorted(total_syn_list,key=lambda x:len(x),reverse=True)[int(len(total_syn_list)*0.5)])
    ###############
    pool = multiprocessing.Pool(threads_num)
    result=[]
    other_result=[]
    for gene_list,modu_list in zip(total_syn_list,total_modu_list):
        if len(gene_list)>=big_num:
            result.append(pool.apply_async(MCL_modu_cal,(gene_list,modu_list,big_num)))
            # result.append(MCL_modu_cal(gene_list,modu_list,big_num)) 
        else:
            other_result.append(gene_list)
    pool.close()
    pool.join()
    wr_right_file=open(right_file,'w')
    for gene_name_list in  result:
        gene_name_list1=gene_name_list.get()
        for i_list in gene_name_list1[1]:
            this_dict={}
            for i in i_list:
                add_gene_dict(i.split(':')[0],re.findall(r':(.*)',i)[0],this_dict)
            for key,value in this_dict.items():
                wr_right_file.write(key+':')
                for x in list(set(value)):
                    wr_right_file.write(x+',')
                wr_right_file.write('\t')
            wr_right_file.write('\n')
    # for gene_name_list in  other_result:
    #     gene_name_list1=gene_name_list
        # print(gene_name_list1)
    for i_list in other_result:
        this_dict={}
        for i in i_list:
            add_gene_dict(i.split(':')[0],re.findall(r':(.*)',i)[0],this_dict)
        for key,value in this_dict.items():
            wr_right_file.write(key+':')
            for x in list(set(value)):
                wr_right_file.write(x+',')
            wr_right_file.write('\t')
        wr_right_file.write('\n')

if __name__ == '__main__':
    # total_syn_list,total_modu_list=cluster_gene_pairs(sys.argv[1],sys.argv[2],sys.argv[3])
    import sys
    out_folder=sys.argv[1]
    out_name=sys.argv[2]
    threads=50
    Total_syn_list,Total_net_list=cluster_gene_pairs(out_folder,out_folder+'/total_temp_syntenic_gene_pairs.txt_test',out_folder+'/total_temp_syntenic_network.txt_test')
    MCL_cluster(Total_syn_list,Total_net_list,out_folder+'/'+out_name,threads)
    #file1=open(sys.argv[1],'r')
    #file2=open(sys.argv[2],'r')
    #total_syn_list,total_modu_list=[],[]
    #for line in file1:
    #    line_list=line.strip().split('\t')[1:]
    #    total_syn_list.append(line_list)
    #for line in file2:
    #    line_list=line.strip().split('\t')[1:]
    #    line_modu=[]
    #    for i in line_list:
    #        line_modu.append([int(i.split(':')[0]),int(i.split(':')[1])])
    #    total_modu_list.append(line_modu)
    #MCL_cluster(total_syn_list,total_modu_list,sys.argv[3],20)

