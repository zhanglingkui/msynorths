import sys
import os
import re
def cluster_of_ref(out_folder):
    total_syn_dict={}
    for syn in os.listdir(out_folder+'mSynF1'):
        if syn.endswith('syn.seg'):
            syn_file=open(out_folder+'/mSynF1/'+syn,'r')
            num1=re.findall('\d+',syn)[0]
            num2=re.findall('\d+',syn)[1]
            for line in syn_file:
                if line.startswith('##'):
                    continue
                line_list=line.strip().split('\t')
                ref_gene='1:'+line_list[0]
                query_gene=num2+':'+line_list[1]
                if ref_gene not in total_syn_dict.keys():
                    total_syn_dict[ref_gene]=[]
                total_syn_dict[ref_gene].append(query_gene)
    out_file=open(out_folder+'/Total_species_syntenic_gene_pairs.txt','w')
    for key,value in total_syn_dict.items():
        out_file.write(key+',\t')
        out_dict={}
        for i in value:
            num=len(i.split(':')[0])
            if i.split(':')[0] not in out_dict:
                out_dict[i.split(':')[0]]=[]
            out_dict[i.split(':')[0]].append(i[num+1:])
        for key1,value1 in out_dict.items():
            out_file.write(key1+':')
            for j in value1:
                out_file.write(j+',')
            out_file.write('\t')
        out_file.write('\n')
                    
        
        
    
    
            
            
    