import collections
from functools import reduce
import matplotlib.pyplot as plt
import matplotlib
import os
import sys
matplotlib.rcParams['pdf.fonttype']=42
def get_density(loop_list):
    win=500
    step=50
    new_list=[[],[]]
    for i in range(0,len(loop_list[1]),step):
        new_list[0].append(i)
        new_list[1].append(sum(loop_list[1][i:i+win])/win)
    return new_list
        
        
def get_pos_dict(pos_file,chr_gap,order_list):
    loop_pos_file=open(pos_file,'r')
    chr_gene_pos_dict=collections.OrderedDict()
    for line in loop_pos_file:
        line_list=line.strip().split('\t')
        if line_list[3] in chr_gene_pos_dict.keys():
            chr_gene_pos_dict[line_list[3]].append([line_list[0],int(line_list[4].split(':')[0])])
        else:
            chr_gene_pos_dict[line_list[3]]=[[line_list[0],int(line_list[4].split(':')[0])]]

    loop_pos_file.close()
    gene_pos_dict={}
    chr_pos_list=[]
    chr_length_dict={}
    chr_start=0
    for key in order_list:
        if key=='':
            continue
        elif key in chr_gene_pos_dict.keys():

            value=chr_gene_pos_dict[key]
        else:
            print(key+' is wrong.')
        index=0
        for i in value:
            index+=1
            # gene_pos_dict[i[0]]=i[1]+chr_start
            gene_pos_dict[i[0]]=index+chr_start
        # chr_pos_list.append([key,chr_start,chr_start+value[-1][1]-value[0][1]])
        # chr_length_dict[key]=value[-1][1]-value[0][1]
        # chr_start+=(value[-1][1]-value[0][1]+chr_gap)
        chr_length_dict[key]=index
        chr_start+=(index+chr_gap)

        # chr_length_dict[key]=1
        # chr_start+=(1+chr_gap)

    return gene_pos_dict,chr_length_dict,chr_gene_pos_dict

def get_syn_gene_dict(ref_dict,query_dict,gene1,gene2):
    if gene1 in ref_dict.keys():
        ref_dict[gene1].append(gene2)
    else:
        ref_dict[gene1]=[gene2]
    if gene2 in query_dict.keys():
        query_dict[gene2].append(gene1)
    else:
        query_dict[gene2]=[gene1]
    

def syn_point_pict(pos_file1,pos_file2,syn_file,x_list,y_list,out_name,point_color,x_file,y_file):
    gene_pos_dict1,chr_pos_dict1,chr_gene_pos_dict1=get_pos_dict(pos_file1,0,x_list)
    gene_pos_dict2,chr_pos_dict2,chr_gene_pos_dict2=get_pos_dict(pos_file2,0,y_list)
    fig = plt.figure(figsize=(12, 12))
    gs = matplotlib.gridspec.GridSpec(6,6)
    ax1 = fig.add_subplot(gs[1:6,0:5],facecolor='#e8e8e8')
    ax2 = fig.add_subplot(gs[0,0:5],facecolor='white')
    ax3 = fig.add_subplot(gs[1:6,5],facecolor='white',)
    scatter_x,scatter_y=[],[]
    loop_syn_file=open(syn_file,'r')
    ref_gene_dict,query_gene_dict={},{}
    color_list=[]
    best_num,line_num=0,0
    for line in loop_syn_file:
        if line.startswith('#'):
            if line_num!=0:
                ration=best_num/line_num
                # if ration>0.8:
                #     ration=0.8
                color_list+=([ration]*line_num)
            try:
                best_num=int(line[6:].strip())
            except ValueError:
                best_num=0
            line_num=0
            continue
        line_list=line.strip().split('\t')
        if line_list[0] in gene_pos_dict1.keys() and line_list[1] in gene_pos_dict2.keys():
            line_num+=1
            scatter_x.append(gene_pos_dict1[line_list[0]])
            scatter_y.append(gene_pos_dict2[line_list[1]])
            get_syn_gene_dict(ref_gene_dict,query_gene_dict,line_list[0],line_list[1])
        if line_list[1] in gene_pos_dict1.keys() and line_list[0] in gene_pos_dict2.keys():
            line_num+=1
            scatter_x.append(gene_pos_dict1[line_list[1]])
            scatter_y.append(gene_pos_dict2[line_list[0]])
            get_syn_gene_dict(ref_gene_dict,query_gene_dict,line_list[1],line_list[0])
    color_list+=([ration]*line_num)
        
    x_length=0
    xticks_list=[[],[]]
    x_density=[[],[]]
    index=0
    
    for chr_name in x_list:
        if chr_name in chr_pos_dict1.keys():
            for gene_list in chr_gene_pos_dict1[chr_name]:
                gene=gene_list[0]
                index+=1
                x_density[0].append(index)
                if gene in ref_gene_dict.keys():
                    x_density[1].append(len(ref_gene_dict[gene]))
                else:
                    x_density[1].append(0)
            x_length+=chr_pos_dict1[chr_name]
            xticks_list[0].append(x_length)
            xticks_list[1].append(chr_name)
            ax1.axvline(x_length,c='white',lw=1)
            ax2.axvline(x_length,c='#caccd1',lw=1)
        else:
            print(chr_name+' is not in genome!!')
            
    Y_length=0
    yticks_list=[[],[]]
    y_density=[[],[]]
    index=0
    for chr_name in y_list:
        if chr_name in chr_pos_dict2.keys():
            for gene_list in chr_gene_pos_dict2[chr_name]:
                gene=gene_list[0]
                index+=1
                y_density[0].append(index)
                
                if gene in query_gene_dict.keys():
                    y_density[1].append(len(query_gene_dict[gene]))
                else:
                    y_density[1].append(0)
            Y_length+=chr_pos_dict2[chr_name]
            yticks_list[0].append(Y_length)
            yticks_list[1].append(chr_name)
            ax1.axhline(Y_length,c='white',lw=1)
            ax3.axhline(Y_length,c='#caccd1',lw=1)
        else:
            print(chr_name+' is not in genome!!')
    # print(y_density)
    loop_syn_file.close()
    x_density=get_density(x_density)
    y_density=get_density(y_density)
    label_list=['0.1','0.2','0.3','0.4','0.5','0.6','0.7']
    # axx=ax1.scatter(scatter_x,scatter_y,color=point_color,s=0.2,alpha = 1)
    axx=ax1.scatter(scatter_x,scatter_y,c=color_list,s=0.2,alpha = 1)
    ax1.set_xlim([0,x_length])
    ax1.set_ylim([0,Y_length])
    ax1.set_yticks(yticks_list[0])
    ax1.set_yticklabels(yticks_list[1],fontsize=15)
    ax1.set_xticks(xticks_list[0])
    ax1.set_xticklabels(xticks_list[1],fontsize=15,rotation=90,)
    ax1.set_xlabel(x_file,fontsize=20)
    ax1.set_ylabel(y_file,fontsize=20)
    
    
    ax2.plot(x_density[0],x_density[1],c='#fe5000')
    ax2.set_ylim([0,1.5])
    ax2.set_xlim([0,x_length])
    ax2.set_xticks([])
    
    ax3.plot(y_density[1],y_density[0],c='#fe5000')
    ax3.set_ylim([0,Y_length])
    ax3.set_xlim([0,1.5])
    ax3.set_yticks([])
    position=fig.add_axes([0.55, 0.01, 0.3, 0.02])
    fig.colorbar(axx,ax=ax1,cax=position,orientation='horizontal')
    plt.savefig(out_name,dpi=300, bbox_inches="tight",pad_inches=0.3)

def run_syn_pict(config_file,msynf,out_name,in_syn_file):
    op_config_file=open(config_file,'r')
    for line in op_config_file:
        line_list=line.strip().split(':')
        if line.startswith('Point_color'):
            point_color=line_list[1].strip()
        elif line.startswith('X-axis'):
            x_file=line_list[1].strip()
        elif line.startswith('X-chr_order'):
            x_list=line_list[1].strip().split(',')
            x_list.append('')
            x_list.remove('')
        elif line.startswith('Y-axis'):
            y_file=line_list[1].strip()
        elif line.startswith('Y-chr_order'):
            y_list=line_list[1].strip().split(',')
            y_list.append('')
            y_list.remove('')
    op_config_file.close()
    for folder in os.listdir(msynf):
        if folder == x_file:
            pos_file1=msynf+'/'+folder+'/species.gff_pos'
        if folder == y_file:
            pos_file2=msynf+'/'+folder+'/species.gff_pos'
    if in_syn_file:
        syn_file=in_syn_file
        
    elif int(x_file[5:])>int(y_file[5:]):
        syn_file=msynf+'/'+y_file+'/species'+y_file[5:]+'_species'+x_file[5:]+'.syn.seg'
    elif int(x_file[5:])==int(y_file[5:]):
        syn_file=msynf+'/'+y_file+'/self.syn'
    else:
        syn_file=msynf+'/'+x_file+'/species'+x_file[5:]+'_species'+y_file[5:]+'.syn.seg'
    # print(syn_file)
    syn_point_pict(pos_file1,pos_file2,syn_file,x_list,y_list,out_name,point_color,x_file,y_file)

if __name__ == '__main__':
    # import sys
    import argparse
    parser = argparse.ArgumentParser(description='Syn_pict')
    parser.add_argument('-f', type=str,help='Syn_pict config file')
    parser.add_argument('-d', type=str, help='mSynOrths out folder')
    parser.add_argument('-o', type=str, help='Out picture file name (.pdf, .png, .svg)')
    parser.add_argument('-s', type=str, help='syn file')
    args = parser.parse_args()
    if args.f and args.d and args.o:
        run_syn_pict(args.f,args.d,args.o,args.s)
    else:
        print('Incomplete parameters!!!')
    # syn_point_pict(sys.argv[1],sys.argv[2],sys.argv[3])
    
    


