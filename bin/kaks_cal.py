
import re
import os
import sys
import multiprocessing
def test_func(a):
    a=2 
    return a
def cal_kaks_func(m1,m2,msynF,outF,gene_pair_file,threads_num):
    num1=re.findall('\d+',m1)[0]
    num2=re.findall('\d+',m2)[0]
    pep1='%s/mSynF%s/species.gff_pep' %(msynF,str(num1))
    cds1='%s/mSynF%s/species.gff_cds' %(msynF,str(num1))
    pep2='%s/mSynF%s/species.gff_pep' %(msynF,str(num2))
    cds2='%s/mSynF%s/species.gff_cds' %(msynF,str(num2))
    if num1<num2:
        syn_file='%s/mSynF%s/species%s_species%s.syn.seg' %(msynF,str(num1),str(num1),str(num2))
    elif num1>num2:
        syn_file='%s/mSynF%s/species%s_species%s.syn.seg' %(msynF,str(num2),str(num2),str(num1))
    elif num1==num2:
        syn_file='%s/mSynF%s/self.syn' %(msynF,str(num2))
    if gene_pair_file:
        syn_file=gene_pair_file
    
    def get_fa_dict(fa_file,out_dict):
        op_fa_file=open(fa_file,'r')
        for line in op_fa_file:
            if line.startswith('>'):
                fa_key=line[1:].strip()
                out_dict[fa_key]=''
            else:
                out_dict[fa_key]+=line.strip()
        op_fa_file.close()
    total_pep_dict={}
    total_cds_dict={}
    if 'self.syn' not in syn_file:
        get_fa_dict(pep1,total_pep_dict)
        get_fa_dict(pep2,total_pep_dict)
        get_fa_dict(cds1,total_cds_dict)
        get_fa_dict(cds2,total_cds_dict)
    else:
        get_fa_dict(pep1,total_pep_dict)
        get_fa_dict(cds1,total_cds_dict)
    op_syn_file=open(syn_file,'r')
    index=0
    os.system('mkdir -p '+outF+'/fa_db')
    for line in op_syn_file:
        index+=1
        out_file=open(outF+'/fa_db/gene'+str(index)+'_cds.fa','w')
        out_file2=open(outF+'/fa_db/gene'+str(index)+'_pep.fa','w')
        if line.startswith('#'):
            continue
        line_list=line.strip().split('\t')
        out_file.write('>'+line_list[0]+'\n'+total_cds_dict[line_list[0]]+'\n')
        out_file.write('>'+line_list[1]+'\n'+total_cds_dict[line_list[1]]+'\n')
        out_file.close()
        out_file2.write('>'+line_list[0]+'\n'+total_pep_dict[line_list[0]]+'\n')
        out_file2.write('>'+line_list[1]+'\n'+total_pep_dict[line_list[1]]+'\n')
        out_file2.close()
    op_syn_file.close()
    
    current_dir=os.path.dirname(sys.argv[0])
    mafft_file=current_dir+'/../software/mafft'
    pal2na_file=current_dir+'/../software/pal2nal.v14/pal2nal.pl'
    kaks_file=current_dir+'/../software/KaKs_Calculator2.0/KaKs_Calculator'
    pool = multiprocessing.Pool(threads_num)
    for fa_file in os.listdir(outF+'/fa_db/'):
        if fa_file.endswith('_pep.fa'):
            result=pool.apply_async(run_cal_kaks_func,(mafft_file,kaks_file,fa_file,pal2na_file,outF))
    result.get()
    pool.close()
    pool.join()
    out_kaks_file=open(outF+'/total_gene_pairs.kaks','w')
    out_kaks_file.write('Sequence\tMethod\tKa\tKs\tKa/Ks\tP-Value(Fisher)\tLength\tS-Sites\tN-Sites\tFold-Sites(0:2:4)\tSubstitutions\tS-Substitutions\tN-Substitutions\tFold-S-Substitutions(0:2:4)\tFold-N-Substitutions(0:2:4)\tDivergence-Time\tSubstitution-Rate-Ratio(rTC:rAG:rTA:rCG:rTG:rCA/rCA)\tGC(1:2:3)\tML-Score\tAICc\tAkaike-Weight\tModel')
    for kaks_file in os.listdir(outF+'/fa_db/'):
        if kaks_file.endswith('pep.fa.kaks'):
            op_ks_file=open(outF+'/fa_db/'+kaks_file,'r')
            for line in op_ks_file:
                if line.startswith('Sequence') or line.strip()=='':
                    continue
                else:
                    out_kaks_file.write(line)
            op_ks_file.close()
            
def AXT_conver_func(in_aln_file,out_axt_file):
        op_in_aln_file=open(in_aln_file,'r')
        op_out_axt_file=open(out_axt_file,'w')
        fa_dict={}
        for line in op_in_aln_file:
            if line.startswith('>'):
                gene_id=line[1:].strip()
                fa_dict[gene_id]=''
            else:
                fa_dict[gene_id]+=line.strip()
        op_in_aln_file.close()
        if len(fa_dict)==2:
            op_out_axt_file.write(list(fa_dict.keys())[0]+'&'+list(fa_dict.keys())[1]+'\n'+fa_dict[list(fa_dict.keys())[0]]+'\n'+fa_dict[list(fa_dict.keys())[1]]+'\n\n')
            op_out_axt_file.close()  
                 
def run_cal_kaks_func(mafft_file,kaks_file,fa_file,pal2na_file,outF):
    op_fa_file=outF+'/fa_db/'+fa_file
    op_cds_file=outF+'/fa_db/'+fa_file[:-6]+'cds.fa'
    op_cds_aln_file=outF+'/fa_db/'+fa_file[:-6]+'cds.fa.aln'
    op_aln_file=outF+'/fa_db/'+fa_file+'.aln'
    op_axt_file=outF+'/fa_db/'+fa_file+'.axt'
    op_kaks_file=outF+'/fa_db/'+fa_file+'.kaks'
    os.system(mafft_file+' --auto --quiet --thread 2 '+op_fa_file+' > '+op_aln_file)
    os.system(pal2na_file+' -output fasta '+op_aln_file+' '+op_cds_file+' > '+op_cds_aln_file)
    AXT_conver_func(op_cds_aln_file,op_axt_file)
    os.system(kaks_file+' -m NG -i '+op_axt_file+' -o '+op_kaks_file)
    
if __name__ == '__main__':
    # import sys
    import argparse
    parser = argparse.ArgumentParser(description='cal_kaks')
    parser.add_argument('-d', type=str, help='mSynOrths out folder')
    parser.add_argument('-m1', type=str, help='single copy file')
    parser.add_argument('-m2', type=str, help='single copy file')
    parser.add_argument('-s', type=str,help='User can provide speceial gene pair file')
    parser.add_argument('-o', type=str,help='out file')
    parser.add_argument('-t', type=int, default=10, help='threads number')
    args = parser.parse_args()
    m1,m2,msynf,synf,thread,out_file=args.m1,args.m2,args.d,args.s,args.t,args.o
    if args.m1 and args.m2 and args.d and args.o:
        cal_kaks_func(m1,m2,msynf,out_file,synf,thread)
    else:
        print('Incomplete parameters!!!')