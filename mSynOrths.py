import argparse
import math
import os
import sys
#### Add self module fold ###
current_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(current_dir+"/bin")
import normal_msynorths
import ref_msynorths


parser = argparse.ArgumentParser(description='mSynOrths_0.1')
parser.add_argument('-f', type=str,help='input genome data location file')
parser.add_argument('-r',  action='store_true',help='Choose the first genome as reference')
parser.add_argument('-c', "--continueDo", action='store_true',
    help="If the program breaks,you can use this parameter to continue")
parser.add_argument('-a', "--addGenome", action='store_true', help="Add the new genome to the existing results")
parser.add_argument('-o', type=str, default=os.getcwd()+'/mSynF_out', help='output folder')
parser.add_argument('-t', type=int, default=10, help='input num of threads')
parser.add_argument('-s', type=str, default='diamond',help='blast or diamond')
parser.add_argument('-n', type=int,default=3, 
    help='The number of flanking homologous gene pairs supporting syntenic gene pairs.')
parser.add_argument('-x', type=int, default=4, help='syntenic segment size, default=4')
parser.add_argument('-m', type=float, default=0.3, help='best synteninc ration, default=0.3')
parser.add_argument('-e', type=float,default=10**-5, help='blast Evalue, default=1e-5')
parser.add_argument('-i', type=float,default=30, help='blast identiy, default=30')
parser.add_argument('-v', type=float,default=0.3, help='blast coverage, default=0.3')
parser.add_argument('-g', type=int,default=50, help='The maximum number of genes in the syntenic fragment gap, default=50')
args = parser.parse_args()
if args.f:
    fa_gff_file = args.f
else:
    print('You must provide genome data location file.')
    sys.exit()

if args.s == 'diamond' or args.s=='blast':
    compare_tools=args.s
else:
    print(args.s+'is wrong, mSynOrths just support diamond and blast.')
    sys.exit()
identity=args.i
coverage=args.v
threads = args.t
out_folder = args.o+'/'
evalue=args.e
flank_gene_num=args.n
gap_num=args.g
seg_num=args.x
best_syn_ration=args.m

if args.continueDo:
    if args.r:
        ref_msynorths.run_continue(fa_gff_file,out_folder,threads,compare_tools,evalue,gap_num,flank_gene_num,seg_num,best_syn_ration,identity,coverage)
    else:
        
        normal_msynorths.run_continue(fa_gff_file,out_folder,threads,compare_tools,evalue,gap_num,flank_gene_num,seg_num,best_syn_ration,identity,coverage)
elif args.addGenome:
    if args.r:
        ref_msynorths.run_add_genome(fa_gff_file,out_folder,threads,compare_tools,evalue,gap_num,flank_gene_num,seg_num,best_syn_ration,identity,coverage)
    else:
        normal_msynorths.run_add_genome(fa_gff_file,out_folder,threads,compare_tools,evalue,gap_num,flank_gene_num,seg_num,best_syn_ration,identity,coverage)
else:
    if args.r:
        ref_msynorths.run_mSynOrths(fa_gff_file,out_folder,threads,compare_tools,evalue,gap_num,flank_gene_num,seg_num,best_syn_ration,identity,coverage)
    else:
        normal_msynorths.run_mSynOrths(fa_gff_file,out_folder,threads,compare_tools,evalue,gap_num,flank_gene_num,seg_num,best_syn_ration,identity,coverage)
