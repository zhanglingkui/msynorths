# mSynOrths
## Install 
```
Install:
conda create -n msynorths python=3.7
git clone https://gitee.com/zhanglingkui/msynorths.git
pip install networkx==2.2
pip install numpy==1.21.6
pip install matplotlib==3.0.2
pip install scikit-learn==1.0.2
pip install scipy==1.1.0
```

## Usage
```
usage: mSynOrths.py [-h] [-f F] [-c] [-a] [-o O] [-t T] [-s S] [-n N] [-e E]
                    [-g G]

mSynOrths_0.1

optional arguments:
  -h, --help        show this help message and exit
  -f F              input genome data location file
  -c, --continueDo  If the program breaks,you can use this parameter to
                    continue
  -a, --addGenome   Add the genome to the existing results
  -o O              output folder
  -t T              input num of threads
  -s S              blast or diamond
  -n N              The number of flanking homologous gene pairs supporting
                    syntenic gene pairs.
  -e E              blast Evalue
  -g G              The maximum number of genes in the syntenic fragment gap
```
## Normal syntenic analyse 
```
python mSynOrths.py -f Config.txt -o out_fold -t 10 -s diamond
```
##### Config.txt file, tell mSynorths where the fasta file and gff file of genome.
```
/path_of_genome_data/species1.fasta,/path_of_genome_data/species1.gff
/path_of_genome_data/species2.fasta,/path_of_genome_data/species2.gff
```

##  Add new genomes to existing results
```
python mSynOrths.py -f Config_new.txt -o out_fold -t 10 -s diamond -a
```
##### Config_new.txt file, add new genome file location.
```
/path_of_genome_data/species1.fasta,/path_of_genome_data/species1.gff
/path_of_genome_data/species2.fasta,/path_of_genome_data/species2.gff
/path_of_genome_data/species3_new.fasta,/path_of_genome_data/species3_new.gff
/path_of_genome_data/species4_new.fasta,/path_of_genome_data/species4_new.gff
```


##  mSynOrths interruption continues to run
```
python mSynOrths.py -f Config.txt -o out_fold -t 10 -s diamond -c
```
